#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

    return 0;
}


list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    cout << "Loading " << filename << "..." << endl;

    ifstream input( filename );

    if ( !input.good() )
    {
        cout << "Error opening file" << endl;
    }

    string line;
    while ( getline( input, line ) )
    {
        bookText.push_back( line );
    }

    cout << endl << bookText.size() << " lines loaded" << endl << endl;

    input.close();

    return bookText;
}


void ReadBook( list<string> bookText )
{
	int pageHeight = 30;
	int counter = 0;
	int totalLines = 0;

	for (list<string>::iterator it = bookText.begin(); it != bookText.end(); it++)
	{
		cout << *it << endl;
		totalLines++;
		counter++;
	

	if (counter == pageHeight)
	{
		//display menu
		Menu::DrawHorizontalBar(5,'*');
		
		cout << "Line " << totalLines << " of " << bookText.size() << endl << endl;

		int choice = Menu::ShowIntMenuWithPrompt({ "Backward","Forward","Exit" }, false);

		

		if (choice == 1)
		{
			for (int i=0; i<pageHeight*2;i++)
			{
				if (it != bookText.begin())
				{
					it--;
					totalLines--;
				}
			}
		}
		else if(choice ==3)
		{
			break;
		}
		counter = 0;
	 }
	
    }
}
