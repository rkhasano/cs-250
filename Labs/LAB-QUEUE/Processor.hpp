#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First served" << endl;
	

	int cycles = 0;

	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << cycles << endl;
		output << "Currently processing Job#" << jobQueue.Front()->id << "\t Time remaining: " << jobQueue.Front()->fcfs_timeRemaining << endl;

		jobQueue.Front()->Work(FCFS);
		
		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->fcfs_finishTime = cycles;
			jobQueue.Pop();
		}
		cycles++;
	}
	output << "sumary" << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime  << endl;
	}

	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin" << endl;

	int cycles = 0;
	int timer = 0;
	 while(jobQueue.Size()>0)
	 {
		output << "Cycle: " << cycles << endl;
		output << "Currently processing Job#" << jobQueue.Front()->id << "\t Time remaining:" << jobQueue.Front()->rr_timeRemaining << endl;
		if(timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;

		}

		jobQueue.Front()->Work(RR);

		if(jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->rr_finishTime = cycles;
			
			jobQueue.Pop();
		}
		cycles++;
		timer++;
	 }
	 output << "sumary" << endl;
	 for (unsigned int i =0; i < allJobs.size(); i++)
	 {
		 output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime <<   "times interupted" << allJobs[i].rr_timesInterrupted <<endl;
	 }

	 output.close();
}

#endif
