// Lab - Standard Template Library - Part 2 - Lists
// Ruslan, Khasanov

#include <iostream>
#include <list>
#include <string>
using namespace std;
void DisplayList(list<string> &states)
{
	for (list<string>::iterator it = states.begin(); it != states.end(); it++)
	{
		cout << *it << "\t";
	}
}

int main()
{
	list<string> states;
	bool done = false;

	while(!done)
	{
		string state;
		cout << "-------------------" << endl
			<< "State list size: " << states.size() << endl;
		cout << endl << "1. Add new state to front. " << "2. Add new state to back. " << endl
			<< "3. Pop front state. " << "4. Pop back state. " << endl
			<< "5. Continue." << endl;
		int choice;
		cin >> choice;

		if (choice ==1)
		{
			string state;
			cout << "Add new state to front " << endl;
			cout << "Enter the name of a state: ";
			cin >> state;
			states.push_front(state);
		}
		else if(choice == 2)
		{
			string state;
			cout << "Add new state to back " << endl;
			cout << "Enter the name of a state: ";
			cin >> state;
			states.push_back(state);

		}
		else if (choice == 3)
		{
			cout << states.front() << "Removed." << endl;
			states.pop_front();
		}
		else if (choice == 4)
		{
			cout << states.back() << "Removed." << endl;
			states.pop_back();
		}
		else 
		{
			//assuming user clicked 5 or any other number
			done = true;
		}
	}
	
	cout << "\nOriginal List" << endl;
	DisplayList(states);

	cout << "\n\nReversed List" << endl;
	states.reverse();
	DisplayList(states);

	cout << "\n\nSorted List" << endl;
	states.sort();
	DisplayList(states);


    cin.ignore();
    cin.get();
    return 0;
}
