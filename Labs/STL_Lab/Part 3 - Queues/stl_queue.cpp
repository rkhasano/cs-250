// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	bool done = false;

	while(!done)
	{
		cout << "------------" << endl;
		cout << "1. Enqueu transaction.   2. Continue.  " << endl;
		int choice;
		cin >> choice;
		if (choice == 1)
		{
			cout << "Enter amount of transaction  ";
			float transaction;
			cin >> transaction;
			transactions.push(transaction);

		}
		else done = true;
	}
	float balance = 0;
	cout << endl;
	while (!transactions.empty())
	{
		cout << transactions.front() << " pushed to account" << endl;
		balance += transactions.front();
		transactions.pop();
	}

	cout << "\nFinal balance is " << balance << endl;

    cin.ignore();
    cin.get();
    return 0;
}
