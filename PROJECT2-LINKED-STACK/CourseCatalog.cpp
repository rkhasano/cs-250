#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );
	Course corse;
	Menu::Header("VIEW COURSES");

	cout << "# Code" << "\t" << "Course" << "\n  Prerequisite" << endl;

	for (int i = 0; i<m_courses.Size(); i++)
	{
		corse = m_courses.Get(i);
		
		cout << i+1 << " "<< corse.code << " " << corse.name << "\n    " << corse.prereq << endl << endl;
	}
}

Course CourseCatalog::FindCourse( const string& code )
{
	Course someCourse;
	for (int i=0;i<m_courses.Size();i++)
	{
		someCourse = m_courses.Get(i);
		if (someCourse.code == code) 
		{
			return someCourse;
		}
	}
	
	{
		throw CourseNotFound("Not yet implemented!");
	}
}

void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );
	Course current;
	string code;
	cout << "Enter the code: ";
	cin >> code;
	try
	{
		current = FindCourse(code);
		
	}
	catch (CourseNotFound)
	{
		cout << "course not found\n";
	}
	LinkedStack<Course> Prereqs;
	Prereqs.Push(current);
	
	cout << "Classes to Take: \n";
	
	
	
	while (current.prereq!="")
	{
		try 
		{
			current = FindCourse(current.prereq);
			Prereqs.Push(current);
		}
		catch (CourseNotFound) 
		{
			break;
		}
	}
	
	while(Prereqs.Size()>0)
	{
		try
		{
			current = Prereqs.Top();
			cout << current.code << " " << current.name << endl;
			Prereqs.Pop();
		}
		catch(CourseNotFound)
		{
			break;
		}
	}

}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
