#ifndef _STACK_HPP
#define _STACK_HPP
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "Node.hpp"

template <typename T>
class LinkedStack
{
public:
	LinkedStack()
	{
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount = 0;
	}

	void Push(const T& newData)
	{
		Node<T> *newPtr = new Node<T>;
		newPtr->data = newData;
		if (m_ptrLast==nullptr)
		{
			
			m_ptrLast = newPtr;
			m_ptrFirst = newPtr;
			
		}
		else
		{
			m_ptrLast->ptrNext = newPtr;
			newPtr->ptrPrev = m_ptrLast;
			m_ptrLast = newPtr;
		}

		

		m_itemCount++;

	}

	T& Top()
	{
		if (m_ptrLast==nullptr)
		{
			
			throw CourseNotFound("Course not Found");
		}
		
		else
		{
			return m_ptrLast->data; // placeholder

	    }

		
	}

	void Pop()
	{
		if (m_ptrLast==nullptr)
		{
			return;
		}

		else if (m_ptrLast == m_ptrFirst)
		{
			
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
			delete m_ptrLast;
		}

		else
		{
			Node<T> *newPtr = new Node<T>;
			newPtr = m_ptrLast->ptrPrev;
			newPtr->ptrNext = nullptr;
			m_ptrLast = newPtr;

			m_itemCount--;
		}
	}

	int Size()
	{
		return m_itemCount;    // placeholder
	}

private:
	Node<T>* m_ptrFirst;
	Node<T>* m_ptrLast;
	int m_itemCount;
};

#endif
