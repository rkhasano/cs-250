#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		if (IsFull())
		{
			cout << "Can't shift right, List is full." << endl;
			return false; 
		}
		else
		{
			for(int i= m_itemCount;i>atIndex;i--)
			{
				m_arr[i] = m_arr[i - 1];
			}
			return true;
		}
    }

    bool ShiftLeft( int atIndex )
    {
		if (IsEmpty())
		{
			cout << "Can't shift left, List is empty." << endl;
			return false; // placeholder
		}
		else
		{
			for (int i = atIndex; i<m_itemCount-1; i++)
			{
				m_arr[i] = m_arr[i + 1];
			}
			return true;
		}

    }

public:
    List()
    {
		//initialize m_itemCount to zero
		m_itemCount = 0;
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
        //return number of items in array
		return m_itemCount; 
    }

    bool    IsEmpty() const
    {
		//returns true or false depending on m_itemCount
		return (m_itemCount == 0);
    }

    bool    IsFull() const
    {
		/*if m_itemCount is equal to Array size it is full(returns true)
		otherwise it is not full(returns false)
		*/
		return (m_itemCount == ARRAY_SIZE);
    }

    bool    PushFront( const T& newItem )
    {
		
		//If array is full we cannot add anymore items, returns false and exit the function.
		if (IsFull())
		{
			return false;
		}

		//if array is not full shift all items one index up
		for(int i=m_itemCount;i>0;i--)
		{
			m_arr[i] = m_arr[i - 1];
		}

		m_arr[0] = newItem;//add new item to index 0;
		m_itemCount++;
		return true;
    }

    bool    PushBack( const T& newItem )
    {
		/*
		If array is full we cannot add anymore items, returns false and exit the function.
		If it is not full add an item to the end of the array and increase itemCount by one.
		*/
		if (IsFull())
		{
			return false;
		}

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
    }

    bool    Insert( int atIndex, const T& item )
    {
        if(this->IsFull())
		{
			return false;
		}
		else if(atIndex>m_itemCount)
		{
			return false;
		}
		else
		{
			ShiftRight(atIndex);
			m_arr[atIndex] = item;
			m_itemCount++;
			return true;
		}
		 
    }

    bool    PopFront()
    {
        if(m_itemCount>0)
		{
			Remove(0);
			return true;
			
		}
		return false; // placeholder
    }

    bool    PopBack()
    {
		if (m_itemCount > 0)
		{
			m_itemCount--;
			return true;
		}
		return false; // placeholder
    }

    bool    RemoveValue( const T& item )
    {
		
		while(this->Contains(item))
		{
			for(int i=0;i<m_itemCount;i++)
			{
				if(m_arr[i]==item)
				{
					this->Remove(i);
				}
			}
		}
        
		return true; // placeholder
    }

    bool    Remove( int atIndex )
    {
		ShiftLeft(atIndex);
		m_itemCount--; //decrease number of elements in array;

		return true; // placeholder
    }

    void    Clear()
    {
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		if (this->IsEmpty())
		{
			return nullptr;
		}
		else 
		{
			return &m_arr[atIndex];
		}
    }

    T*      GetFront()
    {
		if (this->IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return &m_arr[0];
		}
    }

    T*      GetBack()
    {
		if (this->IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return &m_arr[m_itemCount-1];
		}
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
        
			int count = 0;
			
			for (int i = 0; i<m_itemCount; i++)
			{
				if (m_arr[i] == item)
				{
					count++;
				}
			}
	
			return count;
    }

    bool    Contains( const T& item ) const
    {
       for(int i=0;i<m_itemCount;i++)
	   {
		   if (m_arr[i] == item) return true;
	   }

	   return false;
    }

    friend class Tester;
};


#endif
