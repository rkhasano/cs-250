#include "Tester.hpp"

void Tester::RunTests()
{
	Test_Init();
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
	Test_RemoveValue();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}


//-----In It-----------------------
void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

    // Put tests here
	//Test bellow was witten by the instructor
	List<int> testlist;
	int expected = 0;
	int actual = testlist.Size();
	if (actual == expected) { cout << "Pass"; }
	else { cout << "Fail"; }

}


//-----Test ShiftRight--------------------------------
void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Put tests here
	{
		cout << endl << "Test 1" << endl;
		// Test 1
		List<int> testlist;
		testlist.PushBack(1);
		bool expectedValue = true;
		bool actualValue = testlist.ShiftRight(1);

		cout << "Created empty list;inserted 1 item ShiftRight should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << endl << "Test 2" << endl;
		// Test 2
		List<int> testlist;
		
		for(int i=0;i<100;i++)
		{
			testlist.PushBack(i);
		}


		bool expectedValue = false;
		bool actualValue = testlist.ShiftRight(5);

		cout << "Created empty list.Inserted 100 items, Shiftright should return false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << endl << "Test 3. Prerequisite Get()" << endl;
		// Test 23
		List<char> testlist;
		char a = 'a';
		for (int i = 0; i<5; i++)
		{
			testlist.PushBack(a);
			a++;
		}


		char expectedValue[3] = { 'c','d','e' };
		
		bool shift = testlist.ShiftRight(2);//shift right from index 2
		
		char *actualValue[3];
		actualValue[0] = testlist.Get(3);
		actualValue[1] = testlist.Get(4);
		actualValue[2] = testlist.Get(5);

		cout << "Created empty list.Inserted 5 items, Shiftright from index 2. Items from index 3 to 5 should be 'c' 'd' 'e'." << endl;
		
		if ((*actualValue[0]==expectedValue[0])&&(*actualValue[1] == expectedValue[1])&&(*actualValue[2] == expectedValue[2]))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

    // Put tests here
	{
		cout << endl << "Test 1" << endl;
		// Test 1
		List<int> testlist;
		for (int i = 0; i<11; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedValue = true;
		bool actualValue = testlist.ShiftLeft(10);

		cout << "Created empty list;inserted 1 item Shiftleft should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << endl << "Test 2" << endl;
		// Test 2
		List<int> testlist;

		


		bool expectedValue = false;
		bool actualValue = testlist.ShiftLeft(5);

		cout << "Created empty list. Shiftleft should return false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << endl << "Test 3. Prerequisite Get()" << endl;
		// Test 3
		List<char> testlist;
		char a = 'a';
		for (int i = 0; i<5; i++)
		{
			testlist.PushBack(a);
			a++;
		}


		char expectedValue[3] = { 'b','c','d' };

		bool shift = testlist.ShiftLeft(0);//shift right from index 2

		char *actualValue[3];
		actualValue[0] = testlist.Get(0);
		actualValue[1] = testlist.Get(1);
		actualValue[2] = testlist.Get(2);

		cout << "Created empty list.Inserted 5 items, Shiftright from index 2. Items from index 3 to 5 should be 'b' 'c' 'd'." << endl;

		if ((*actualValue[0] == expectedValue[0]) && (*actualValue[1] == expectedValue[1]) && (*actualValue[2] == expectedValue[2]))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//------Test_Size----------------------------------------------
void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end
	
	{   // Test begin
		cout << endl << "Test 3" << endl;
		List<int> testList;

		//create an integer variable and initiate it to 101
		int count = 101;
		
		//Try to insert 101 element in testList
		for(int i = 0; i < count;i++ )
		{
			testList.PushBack(i);
		}

		int expectedSize = 100;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 4" << endl;
		List<int> testList;

		//create an integer variable and initiate it to 5
		int count = 5;

		//Try to insert 5 element in testList
		for (int i = 0; i < count; i++)
		{
			testList.PushBack(i);
		}

		//Remove 2 items
		testList.PopBack();
		testList.PopBack();

		int expectedSize = 3;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}


//-----------Test_IsEmpty---------------------------------------------------------------
void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    // Put tests here
	{
		cout << endl << "Test 1" << endl;
		// Test 1
		List<int> testlist;

		bool expectedValue = true;
		bool actualValue = testlist.IsEmpty();

		cout << "Created empty list; IsEmpty() should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;
		cout << "Prerequisites: PushBack()" << endl;
		List<int> testlist;

		testlist.PushBack(1);

		bool expectedValue = false;
		bool actualValue = testlist.IsEmpty();

		cout << "Created list, added 1 item, IsEmpty() should be false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 3
		cout << endl << "Test 3" << endl;
		cout << "Prerequisites: PushBack(),PushFront(),PopBack(),PopFront()" << endl;
		List<int> testlist;

		testlist.PushFront(1);
		testlist.PushBack(10);
		testlist.PopBack();
		testlist.PopFront();

		bool expectedValue = true;
		bool actualValue = testlist.IsEmpty();

		cout << "Created list, added 2 items,poped 2 items, IsEmpty() should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//---------Test IsFull---------------------------------------------------
void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Put tests here
	{
		cout << endl << "Test 1" << endl;
		// Test 1
		List<int> testlist;

		bool expectedValue = false;
		bool actualValue = testlist.IsFull();

		cout << "Created empty list; IsFull() should be false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2" << endl;
		// Test 2
		List<int> testlist;

		for(int i=0;i<100;i++)
		{
			testlist.PushBack(i);
		}

		bool expectedValue = true;
		bool actualValue = testlist.IsFull();

		cout << "Created empty list; Inserted 100 items,IsFull() should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		cout << endl << "Test 3" << endl;
		// Test 3
		List<int> testlist;

		for (int i = 0; i<101; i++)
		{
			testlist.PushBack(i);
		}

		bool expectedValue = true;
		bool actualValue = testlist.IsFull();

		cout << "Created empty list; Inserted 101 items,IsFull() should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}


//-------Test PushFront--------------------------
void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Put tests here
	{
		//Test 1
		cout << endl << "Test 1" << endl;
		cout << "Prerequisite: Get()" << endl;

		List<char> testlist;
		testlist.PushFront('a');
		testlist.PushFront('b');
		testlist.PushFront('c');


		char expectedValues[3] = {
			'c',
			'b',
			'a'
		};
		char* actualValues[3] = {
			testlist.Get(0),
			testlist.Get(1),
			testlist.Get(2)
		};

		// Leave the test before we de-reference the pointers
		// so that it doesn't crash.
		if (actualValues[0] == nullptr || actualValues[1] == nullptr, actualValues[2] == nullptr)
		{
			cout << "Got nullptrs; avoid segfault. Returning" << endl;
			return;
		}

		if (*actualValues[0] == expectedValues[0] &&
			*actualValues[1] == expectedValues[1] &&
			*actualValues[2] == expectedValues[2])
		{
			cout << "Pass" << endl;


		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		//Test 2
		cout << endl << "Test 2" << endl;
		cout << "Create a list.Pushfront 100 items.Try to pushfront 101st item. Push Back should be false." << endl;

		List<char> testlist;
		for (int i = 0; i < 100;i++)
		{
			char a = '0';
			testlist.PushFront(a);
			a++;
		}


		bool expectedValue = false;
		bool actualValue = testlist.PushFront('k');

		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;


		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		//Test 3
		cout << endl << "Test 3" << endl;
		cout << "Create a list.Pushfront 100 items.Size should be 100. ." << endl;

		List<char> testlist;
		for (int i = 0; i < 100; i++)
		{
			char a = '0';
			testlist.PushFront(a);
			a++;
		}


		int expectedValue = 100;
		int actualValue = testlist.Size();

		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;


		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//-------------Test PushBack------------------------------
void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<string> testlist;

		bool expectedValue = true;
		bool actualValue = testlist.PushBack("A");

		cout << "Create a list, PushBack 1 item, should be successful" << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;
		cout << "Prerequisite: Size()" << endl;

		List<string> testlist;
		testlist.PushBack("B");

		int expectedValue = 1;
		int actualValue = testlist.Size();

		cout << "Create a list, PushBack 1 item, Size() should be 1" << endl;

		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 3
		cout << endl << "Test 3" << endl;
		cout << "Prerequisite: Get()" << endl;

		List<char> testlist;
		testlist.PushBack('a');
		testlist.PushBack('b');
		testlist.PushBack('c');


		char expectedValues[3] = {
			'a',
			'b',
			'c'
		};
		char* actualValues[3] = {
			testlist.Get(0),
			testlist.Get(1),
			testlist.Get(2)
		};

		// Leave the test before we de-reference the pointers
		// so that it doesn't crash.
		if (actualValues[0] == nullptr || actualValues[1] == nullptr, actualValues[2] == nullptr)
		{
			cout << "Got nullptrs; avoid segfault. Returning" << endl;
			return;
		}

		if (*actualValues[0] == expectedValues[0] &&
			*actualValues[1] == expectedValues[1] &&
			*actualValues[2] == expectedValues[2])
		{
			cout << "Pass" << endl;
			
		
		}
		else
		{
			cout << "Fail" << endl;
		}
		
	}
	{
		// Test 4
		cout << endl << "Test 4" << endl;
		cout << "Prerequisite: Get()" << endl;

		List<string> testlist;
		testlist.PushBack("Andrew");
		testlist.PushBack("AbduHasan");
		testlist.PushBack("Maria");


		string expectedValues[3] = {
			"Andrew",
			"AbduHasan",
			"Maria"
		};
		string* actualValues[3] = {
			testlist.Get(0),
			testlist.Get(1),
			testlist.Get(2)
		};

		// Leave the test before we de-reference the pointers
		// so that it doesn't crash.
		if (actualValues[0] == nullptr || actualValues[1] == nullptr, actualValues[2] == nullptr)
		{
			cout << "Got nullptrs; avoid segfault. Returning" << endl;
			return;
		}

		if (*actualValues[0] == expectedValues[0] &&
			*actualValues[1] == expectedValues[1] &&
			*actualValues[2] == expectedValues[2])
		{
			cout << "Pass" << endl;


		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}


//--------------Test_PopFront---------------
void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<string> testlist;
		testlist.PushBack("One");
		testlist.PushBack("Two");
		testlist.PushBack("Three");

		bool expectedValue = true;
		bool actualValue = testlist.PopFront();

		cout << "Create a list, PushBack 3 items,PopFront one item should be successful" << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<string> testlist;
		testlist.PushBack("One");
		testlist.PushBack("Two");
		testlist.PushBack("Three");
		testlist.PopFront();
		
		int expectedValue = 2;
		int actualValue = testlist.Size();

		cout << "Create a list, PushBack 3 items,PopFront one item.Size should be 2" << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;
		testlist.PushBack('A');
		testlist.PushBack('Z');
		testlist.PushBack('M');
		testlist.PopFront();

		char expectedValue = 'Z';
		char* actualValue = testlist.GetFront();

		cout << "Create a list, PushBack 3 items,PopFront one item.Get front should return 'C'" << endl;
		if (expectedValue == *actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}


//-----------Test_PopBack--------------------------
void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<string> testlist;
		testlist.PushBack("One");
		

		bool expectedValue = true;
		bool actualValue = testlist.PopBack();

		cout << "Create a list, PushBack 1 item,Popback one item should be successful" << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<string> testlist;
		


		bool expectedValue = false;
		bool actualValue = testlist.PopBack();

		cout << "Create an empty list, Popback one item should be false" << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<int> testlist;
		testlist.PushBack(30);
		testlist.PushBack(50);
		testlist.PushBack(70);
		testlist.PopBack();
		testlist.PopBack();

		int expectedValue = 1;
		int actualValue = testlist.Size();

		cout << "Create a list, PushBack 3 items,Popback two items. Size should be equal to 1." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//---------Test_Clear------------------
void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;
		testlist.PushBack(30);
		testlist.PushBack(50);
		testlist.PushBack(70);
		testlist.Clear();

		bool expectedValue = false;
		bool actualValue = testlist.PopFront();

		cout << "Create a list, PushBack 3 items,Call clear(). Try to popfront(), it should not be successful." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<int> testlist;
		
		for(int i=0;i<100;i++)
		{
			testlist.PushBack(i);
		}
		testlist.Clear();
		testlist.PushBack(101);

		int expectedValue = 1;
		int actualValue = testlist.Size();

		cout << "Create a list, PushBack 100 items.Call clear. Pushback one item. Size should be equal to 1." << endl;
		cout << "Expected size: " << expectedValue << " Actual size: " << actualValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<int> testlist;

		for (int i = 0; i<100; i++)
		{
			testlist.PushBack(i);
		}
		testlist.Clear();
		

		int expectedValue = 0;
		int actualValue = testlist.Size();

		cout << "Create a list, PushBack 100 items.Call clear. Size should be equal to 0." << endl;
		cout << "Expected size: " << expectedValue << " Actual size: " << actualValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//----------Test_Get-------------------------
void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;
		testlist.PushBack(10);
		testlist.PushBack(20);
		testlist.PushBack(30);
		testlist.PushBack(40);
		testlist.PushBack(50);
		
		int expectedOutput = 30;
		int* actualValue = testlist.Get(2);
		if(expectedOutput==(*actualValue))
		{
			cout << "Pass" << endl;
		}
		else 
		{
			cout << "Fail" << endl << "Actual value: " << *actualValue << " Expected value: " << expectedOutput << endl;
		}
	}
	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<int> testlist;
		
		int* expectedOutput = nullptr;
		int* actualValue = testlist.Get(2);
		
		if (expectedOutput == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl << "Actual value: " << *actualValue << " Expected value: " << expectedOutput << endl;
		}
	}
	
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<int> testlist;
		
		for(int i=0;i<100;i++)
		{
			testlist.PushFront(i + 1);
		}

		int expectedOutput = 1;
		int* actualValue = testlist.Get(99);

		if (expectedOutput == (*actualValue))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl << "Actual value: " << *actualValue << " Expected value: " << expectedOutput << endl;
		}
	}
}


//--------Test_GetFront---------------------
void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;
		


		int* expectedValue = nullptr;
		int* actualValue = testlist.GetFront();

		cout << "Create an empty list, GetFront returns nullptr." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<int> testlist;

		for(int i=0;i<100;i++)
		{
			testlist.PushFront(i + 1);
		}

		int expectedValue = 100;
		int* actualValue = testlist.GetFront();

		cout << "Create an empty list,push front 100 items, GetFront should be 100." << endl;
		cout << "Actual value: " << *actualValue << " expected Value: " << expectedValue << endl;
		if (expectedValue == *actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<int> testlist;

		for (int i = 0; i<10; i++)
		{
			testlist.PushBack(i + 1);
		}

		int expectedValue = 1;
		int* actualValue = testlist.GetFront();

		cout << "Create an empty list,push front 10 items, GetFront should be 1." << endl;
		cout << "Actual value: " << *actualValue << " expected Value: " << expectedValue << endl;
		if (expectedValue == *actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}


//----------Test_GetBack-----------------------
void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;



		int* expectedValue = nullptr;
		int* actualValue = testlist.GetBack();

		cout << "Create an empty list, GetFront returns nullptr." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;

		testlist.PushBack('a');
		testlist.PushBack('b');

		char expectedValue = 'b';
		char* actualValue = testlist.GetBack();

		cout << "Create an empty list,pushback 'a' and 'b', GetBack returns 'b'." << endl;
		cout << "Actual value: " << *actualValue << " expected value: " << expectedValue << endl;
		if (expectedValue == *actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;

		testlist.PushFront('a');
		testlist.PushFront('b');
		testlist.PushFront('c');

		char expectedValue = 'a';
		char* actualValue = testlist.GetBack();

		cout << "Create an empty list,pushback 'a', 'b','c', GetBack returns 'a'." << endl;
		cout << "Actual value: " << *actualValue << " expected value: " << expectedValue << endl;
		if (expectedValue == *actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

//------------Test GetCountof-------------------
void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<char> testlist;
		

		int expectedValue = 0;
		int actualValue = testlist.GetCountOf('A');

		cout << "Create an empty list. Try to count 'A's in it. Output should be 0" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;
		char t = 'T';

		for(int i=0;i<75;i++)
		{
			testlist.PushBack(t);
		}
		t = 'a';
		for(int i=0;i<25;i++)
		{
			testlist.PushFront(t);
		}


		int expectedValue = 75;
		int actualValue = testlist.GetCountOf('T');

		cout << "Create an empty list. Insert 75 'T' characters.Count number of 'T' in it. Output should be 75" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;
		testlist.PushBack('A');
		testlist.PushBack('B');
		testlist.PushBack('C');

		int expectedValue = 0;
		int actualValue = testlist.GetCountOf('D');

		cout << "Create an empty list.Push back 'A','B','C'. Try to count 'D's in it. Output should be 0" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 4
		cout << endl << "Test 4" << endl;

		List<char> testlist;
		for(int i=0;i<105;i++)
		{
			testlist.PushBack('A');
		}

		int expectedValue = 100;
		int actualValue = testlist.GetCountOf('A');

		cout << "Create an empty list.Push back 'A' 105 times. Try to count 'A's in it. Output should be 100" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}



//---------Test Contains------------------------
void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<char> testlist;


		bool expectedValue = false;
		bool actualValue = testlist.Contains('A');

		cout << "Create an empty list. Try to find 'A' in it. Output should be false" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;
		char t = 'T';

		for (int i = 0; i<75; i++)
		{
			testlist.PushBack(t);
		}
		t = 'a';
		for (int i = 0; i<25; i++)
		{
			testlist.PushFront(t);
		}


		bool expectedValue = true;
		bool actualValue = testlist.Contains('T');

		cout << "Create an empty list. Insert 75 'T' characters.Check if 'T' is in it. Output should be true." << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;
		testlist.PushBack('A');
		testlist.PushBack('B');
		testlist.PushBack('C');

		bool expectedValue = false;
		bool actualValue = testlist.Contains('D');

		cout << "Create an empty list.Push back 'A','B','C'. Try to find 'D's in it. Output should be 0" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 4
		cout << endl << "Test 4" << endl;

		List<char> testlist;
		for (int i = 0; i<100; i++)
		{
			testlist.PushBack('A');
		}

		bool expectedValue = true;
		bool actualValue = testlist.Contains('A');

		cout << "Create an empty list.Push back 'A' 100 times. Try to check if 'A' is in it. Output should be true" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}


//---------Test Remove------------------------
void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;

		for(int i=0;i<100;i++)
		{
			testlist.PushBack(i+1);
		}
		testlist.Remove(5);

		bool expectedValue = false;
		bool actualValue = testlist.Contains(6);

		cout << "Create a list.Pushback 100 items. Remove item at index 5(number 6)."
			<< " Use contain function to check if 6 is in the list.Output should be false" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;
		testlist.PushBack('A');
		testlist.PushBack('Z');
		testlist.PushBack('C');
		testlist.PushBack('B');
		testlist.PushBack('Y');
		
		testlist.Remove(2);

		int expectedValue = 0;
		int actualValue = testlist.GetCountOf('C');

		cout << "Create a list.Pushback 'A','Z','C','B','Y'. Remove item at index 2('C')."
			<< " Use countof function to check how many 'C' is in the list.Output should be 0" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;
		for(int i=0;i<100;i++)
		{
			testlist.PushBack('z');
		}

		testlist.Remove(50);

		int expectedValue = 99;
		int actualValue = testlist.GetCountOf('z');

		cout << "Create a list.Pushback 'z' 100 times. Remove item at index 50."
			<< " Use countof function to check how many 'z' is in the list.Output should be 99" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 4
		cout << endl << "Test 4" << endl;

		List<char> testlist;
		testlist.PushBack('a');

		testlist.Remove(0);

		int expectedValue = 0;
		int actualValue = testlist.Size();

		cout << "Create a list.Pushback 1 item. Remove  the item."
			<< " Use size function to see the size.Output should be 0" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 5
		cout << endl << "Test 5" << endl;

		List<char> testlist;
		for (int i = 0; i<100; i++)
		{
			testlist.PushBack('z');
		}

		testlist.Remove(99);

		int expectedValue = 99;
		int actualValue = testlist.Size();

		cout << "Create a list.Pushback 100 times. Remove item at index 99."
			<< " Use size function to check the size.Output should be 99" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

//---------Test Remove Value------------------------
void Tester::Test_RemoveValue()
{
	DrawLine();
	cout << "TEST: Test_RemoveValue" << endl;

	// Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<int> testlist;

		for (int i = 0; i<100; i++)
		{
			testlist.PushBack(i + 1);
		}
		testlist.RemoveValue(50);

		bool expectedValue = false;
		bool actualValue = testlist.Contains(50);

		cout << "Create a list.Pushback 100 items. Remove number 50 from the list."
			<< " Use contain function to check if 50 is in the list.Output should be false" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;

		for (int i = 0; i<50; i++)
		{
			testlist.PushBack('K');
		}
		for(int i=0;i<50;i++)
		{
			testlist.PushBack('L');
		}

		testlist.RemoveValue('K');
		testlist.PushFront('K');

		int expectedValue = 1;
		int actualValue = testlist.GetCountOf('K');

		cout << "Create a list.Pushback 50 'K' and 50'L'. Remove all 'K', then add only one 'K' "
			<< " Use getcountof function to count 'K'. Output should be 1" << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue  <<  " Size:" <<testlist.Size() << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;

		char a = 'A';
		for (int i = 0; i<26; i++)
		{
			testlist.PushBack(a);
			a++;
		}
		

		testlist.RemoveValue('A');
		testlist.PushFront('a');

		int expectedValue1 = 0;
		int expectedValue2 = 1;
		int actualValue1 = testlist.GetCountOf('A');
		int actualValue2 = testlist.GetCountOf('a');

		cout << "Create a list.Pushback 26 items(A-Z). Remove 'A', pushfront'a' "
			<< " Use getcountof function to count 'A' and 'a'. Output should be 0 and 1" << endl;
		cout << "Actual value1: " << actualValue1 << " Expected value1: " << expectedValue1 << endl;
		cout << "Actual value2: " << actualValue2 << " Expected value2: " << expectedValue2 << endl;
		if ((expectedValue1 == actualValue1)&&(expectedValue2 == actualValue2))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}



//---------------Test Insert------------------------
void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;

		List<char> testlist;

		testlist.PushBack('A');
		testlist.PushBack('B');
		testlist.PushBack('C');

		

		bool expectedValue = false;
		bool actualValue = testlist.Insert(5, 'D');

		cout << "Create a list.Pushback 'A','B','C'. Insert(5,'D')."
			<< " Should return false, because not contiguous." << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 2
		cout << endl << "Test 2" << endl;

		List<char> testlist;

		testlist.PushBack('A');
		testlist.PushBack('B');
		testlist.PushBack('C');


		testlist.Insert(1, 'D');

		int expectedValue = 4;
		int actualValue = testlist.Size();
		char* index1 = testlist.Get(1);


		cout << "Create a list.Pushback 'A','B','C'. Insert(1,'D')."
			<< " Size should become 4, item at index 1 should be 'D'." << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		cout << "Size: " << actualValue << " item at index 1: " << *index1 << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;

		List<char> testlist;

		for(int i=0;i<0;i++)
		{
			testlist.PushBack('A');
		}



		bool expectedValue = false;
		bool actualValue = testlist.Insert(1, 'E');

		cout << "Create a list.Pushback 100 items. Insert(1,'E')."
			<< " Should return false, because list is full." << endl;
		cout << "Actual value: " << actualValue << " Expected value: " << expectedValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}
